var request = require('supertest')
var app = require('../src/index.js') // Link to your server file


describe('Test / api', () => {
  it('Get', async () => {
    const res = await request(app)
      .get('/')

    expect(res.statusCode).toEqual(200)

  })
})

describe('Test /sendmail api', () => {
  it('Sending 1', async () => {
    const apiParams =  {
      to: "noliuz@gmail.com",
      from: "noliuz@gmail.com",
      message: "Hey Man!!!"
    }

    const res = await request(app).post({
      url: '/sendmail',
      port: 3001,
      headers : {"Content-Type": "application/json"},
      json: apiParams
    })

    expect(res.statusCode).toEqual(200)

  })
})


    /*
    const apiParams =  {
      to: "noliuz@gmail.com",
      from: "noliuz@gmail.com",
      message: "Hey Man!!!"
    }
    const res = await request(app).post({
      url: '/sendmail',
      headers : {"Content-Type": "application/json"},
      json: apiParams
    })

    expect(res).toBe('ok')
    done()*/


/*
  test('should return ok', () => {
    const apiParams =  {
      to: "noliuz@gmail.com",
      from: "noliuz@gmail.com",
      message: "Hey Man!!!"
    }

    request.post({
      url: 'http://localhost:3001/sendmail',
      "Content-Type": "application/json",
      json: {
        to : "noliuz@gmail.com",
        from :"noliuz@gmail.com",
        message : "Ok guy let s test"
      }
    },function (error, response, body) {
      let res
      if (error) {
        res = 'error: '+error;
      } else {
        res = 'ok'
      }
      expect(res).toBe('ok')
    })

  })
*/
