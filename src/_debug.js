let netIncome = 9000000
let tax = 0
if (netIncome <= 150000)
  tax = 0
else if (netIncome >= 150001 && netIncome <= 300000)
  tax = netIncome * 5/100
else if (netIncome >= 300001 && netIncome <= 500000)
  tax = netIncome * 10/100
else if (netIncome >= 500001 && netIncome <= 750000)
  tax = netIncome * 15/100
else if (netIncome >= 750001 && netIncome <= 1000000)
  tax = netIncome * 20/100
else if (netIncome >= 1000001 && netIncome <= 2000000)
  tax = netIncome * 25/100
else if (netIncome >= 2000001 && netIncome <= 5000000)
  tax = netIncome * 30/100
else if (netIncome >= 5000001)
  tax = netIncome * 35/100

console.log(tax)
