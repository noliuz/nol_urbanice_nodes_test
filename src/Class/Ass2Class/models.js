const Sequelize = require('sequelize');
const {seqConn} = require('./seqConn')

const GroupModel = seqConn.define('group', {
  name: {
    type: Sequelize.STRING
  }
}, {
}),

ContactModel = seqConn.define('contact', {
  name: {
    type: Sequelize.STRING
  },
  fname: {
    type: Sequelize.STRING
  },
  lname: {
    type: Sequelize.STRING
  },
  birthdate: {
    type: Sequelize.DATE
  },
  group_id: {
    type:Sequelize.BIGINT
  },
  image_file: {
    type: Sequelize.STRING
  }


}, {
}),
PhoneModel = seqConn.define('phone', {
  number: {
    type: Sequelize.STRING
  },
  contact_id: {
    type: Sequelize.BIGINT
  }
}, {
}),
EmailModel = seqConn.define('email', {
  email: {
    type: Sequelize.STRING
  },
  contact_id: {
    type: Sequelize.BIGINT
  }
}, {
}),
UrlModel = seqConn.define('url', {
  url: {
    type: Sequelize.STRING
  },
  contact_id: {
    type: Sequelize.BIGINT
  }
}, {
})

GroupModel.hasMany(ContactModel,{foreignKey: 'group_id'})
ContactModel.belongsTo(GroupModel,{foreignKey: 'group_id'})

module.exports = {GroupModel, ContactModel,PhoneModel,EmailModel,
        UrlModel}
