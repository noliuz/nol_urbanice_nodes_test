const Sequelize = require('sequelize');
const {seqConn} = require('./seqConn')
const {ContactModel,PhoneModel} = require('./models');

class PhoneClass {
  create(contact_id,number) {
    PhoneModel.create({ contact_id: contact_id,
                        number:number})
              .then(num => {
      console.log("new ID", num.id)
    })
  }

  update(id,contact_id,number) {
    PhoneModel.update({ number: number,contact_id:contact_id }, {
      where: {
        id:id
      }
    }).then(() => {
      console.log("Update phone done.")
    })
  }

  delete(id) {
    PhoneModel.destroy({
      where: {
        id: id
      }
    }).then(() => {
      console.log("delete phone done")
    })
  }

  readByContactId(contact_id,callback) {
    PhoneModel.findAll({ where: { contact_id:contact_id  } })
              .then(contacts => {
      callback(contacts)
      console.log("read phone by contact", JSON.stringify(contacts));
    })
  }

}

module.exports = PhoneClass
