const Sequelize = require('sequelize');
const {GroupModel,ContactModel} = require('./models');
const {seqConn} = require('./seqConn')

class GroupClass {
  create(name) {
    GroupModel.create({ name: name }).then(name => {
      console.log("FAmily's auto-generated ID", name.id)
    })
  }

  update(id,name) {
    GroupModel.update({ name: name }, {
      where: {
        id:id
      }
    }).then(() => {
      console.log("Done")
    })
  }

  delete(id) {
    GroupModel.destroy({
      where: {
        id: id
      }
    }).then(() => {
      console.log("Done")
    })
  }

  readAll(callback) {
    GroupModel.findAll().then(groups => {
      callback(groups)      
    })
  }

  readForGroupListPage(callback) {
    const query = {
      include: [{
        model: ContactModel,
        as: 'contacts',
        required: false
      }]
    }
    GroupModel.findAll(query).then(groups => {
      let list = Array()
      groups.forEach(el =>{
        //console.log(JSON.stringify(el.contacts.length))
        let one = new Object()
        one.id = el.id
        one.name = el.name
        one.count = el.contacts.length

        list.push(one)
      })
      callback(list)
    })
  }

}

module.exports = GroupClass
