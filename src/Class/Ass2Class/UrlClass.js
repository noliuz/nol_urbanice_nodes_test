const Sequelize = require('sequelize');
const {seqConn} = require('./seqConn')
const {UrlModel} = require('./models');

class UrlClass {
  create(contact_id,url) {
    UrlModel.create({ contact_id: contact_id,
                        url:url})
              .then(url => {
      console.log("new url ID", url.id)
    })
  }

  update(id,contact_id,url) {
    UrlModel.update({ url: url,contact_id:contact_id }, {
      where: {
        id:id
      }
    }).then(() => {
      console.log("Update url done.")
    })
  }

  delete(id) {
    UrlModel.destroy({
      where: {
        id: id
      }
    }).then(() => {
      console.log("delete url done")
    })
  }

  readByContactId(contact_id,callback) {
    UrlModel.findAll({ where: { contact_id:contact_id  } })
              .then(contacts => {
      callback(contacts)
      console.log("read url by contact", JSON.stringify(contacts));
    })
  }

}

module.exports = UrlClass
