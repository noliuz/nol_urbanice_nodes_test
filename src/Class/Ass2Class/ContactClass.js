const Sequelize = require('sequelize')
const {seqConn} = require('./seqConn')
const {GroupModel,ContactModel} = require('./models');


class ContactClass {
  constructor() {

  }
  create(name,fname,lname,birthdate,group_id,image_file) {
    ContactModel.create({name,fname,lname,birthdate,group_id,image_file }).then(contact => {
      console.log("new contact id", contact.id)
    })
  }

  update(id,name,fname,lname,birthdate,group_id,image_file) {
    let updatingData = {
      name:name,fname:fname,lname:lname,birthdate:birthdate,
      group_id:group_id,image_file:image_file
    }
    if (name == '')
      delete updatingData['name']
    if (fname == '')
      delete updatingData['fname']
    if (lname == '')
      delete updatingData['lname']
    if (birthdate == '')
      delete updatingData['birthdate']
    if (group_id == '')
      delete updatingData['group_id']
    if (image_file == '')
      delete updatingData['image_file']

    ContactModel.update(updatingData, {
      where: {
        id:id
      }
    }).then(() => {
      console.log("Done")
    })
  }

  delete(id) {
    ContactModel.destroy({
      where: {
        id: id
      }
    }).then(() => {
      console.log("Done")
    })
  }

  readContactByGroupId(group_id,callback) {
    ContactModel.findAll({ where: { group_id:group_id  } })
              .then(contacts => {
      let list = Array()
      contacts.forEach(el => {
        let one = new Object()
        one.name = el.name
        list.push(one)
      })

      callback(list)

    })
  }

  readAll(callback) {
    const query = {
      include: [{
        model: GroupModel,
        as: 'group',
        required: false
      }]
    }
    ContactModel.findAll(
      query
      ).then(contacts => {
      callback(contacts)      
    })
  }

}

module.exports = ContactClass
