const Sequelize = require('sequelize');
const {seqConn} = require('./seqConn')
const {ContactModel,EmailModel} = require('./models');

class EmailClass {
  create(contact_id,email) {
    EmailModel.create({ contact_id: contact_id,
                        email:email})
              .then(email => {
      console.log("new email ID", email.id)
    })
  }

  update(id,contact_id,email) {
    EmailModel.update({ email: email,contact_id:contact_id }, {
      where: {
        id:id
      }
    }).then(() => {
      console.log("Update email done.")
    })
  }

  delete(id) {
    EmailModel.destroy({
      where: {
        id: id
      }
    }).then(() => {
      console.log("delete email done")
    })
  }

  readByContactId(contact_id,callback) {
    PhoneModel.findAll({ where: { contact_id:contact_id  } })
              .then(contacts => {
      callback(contacts)
      console.log("read phone by contact", JSON.stringify(contacts));
    })
  }

}

module.exports = EmailClass
