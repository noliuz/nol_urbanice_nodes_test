const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const helmet = require('helmet')
const morgan = require('morgan')
var request = require('request')
var multer = require('multer');


const Ass1Class = require('./Class/Ass1Class')

const ContactClass = require('./Class/Ass2Class/ContactClass')
const EmailClass = require('./Class/Ass2Class/EmailClass')
const GroupClass = require('./Class/Ass2Class/GroupClass')
const PhoneClass = require('./Class/Ass2Class/PhoneClass')
const UrlClass = require('./Class/Ass2Class/UrlClass')

const Ass3Class = require('./Class/Ass3Class')

const app = express();
module.exports = app

//start plugin
app.use(helmet())
app.use(bodyParser.json())
app.use(cors())
app.use(morgan('combined'))

//assignment 1
app.post('/sendmail', (req, res) => {
  //prepare params
  let data = req.body;
  let apiParams = {
    "personalizations": [
      {
        "to": [
          {
            "email": data.to,
            "name": ""
          }
        ],
        "subject": data.message.substr(0,20)
      }
    ],
    "from": {
      "email": data.from,
      "name": ""
    },
    "reply_to": {
      "email": data.from,
      "name": ""
    },
    "template_id": data.templateID+"",
    "content": [{
                 "type": "text/plain",
                  "value": data.message
               }]
  }
  if (!data.templateID)
    delete apiParams['template_id']

  myAss1 = new Ass1Class()
  myAss1.sendMailPostAPI(apiParams,res)

})
////end assignment 1

////assignment 2, I bypass API Token verification because I don t have authentication process
  //upload avatar for create new contact
    //Multer STORAGE
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './images/')
  },
  filename: function (req, file, cb) {
    cb(null, 'fake_api_token')
  }
})
var upload = multer({ storage: storage })

app.post('/ass2/upload', upload.single('avatar'), function (req, res, next) {
  if (!req.file)
    res.send('error')
  else
    res.send('ok')
})

  //group
const groupClassIns = new GroupClass()
app.post('/ass2/group/new',(req, res) => {
  groupClassIns.create(req.body.name)
})
app.post('/ass2/group/update',(req, res) => {
  groupClassIns.update(req.body.id,req.body.name)
})
app.get('/ass2/group/getlist',(req, res) => {
  groupClassIns.readForGroupListPage(function(list) {
    res.send(list)
  })
})
  //Contacts
const contactClassIns = new ContactClass()
app.get('/ass2/contact/getlist',(req, res) => {
  contactClassIns.readContactByGroupId(req.query.id,function(list) {
    res.send(list)
  })
})
app.post('/ass2/contact/new',(req, res) => {
  name = req.body.name
  fname = req.body.fname
  lname = req.body.lname
  birthdate = req.body.birthdate
  group_id = req.body.group_id
  image_file = req.body.image_file //fake_api_token
  contactClassIns.create(name,fname,lname,birthdate,group_id,image_file)
})
app.post('/ass2/contact/update',(req, res) => {
  id = req.body.id
  name = req.body.name
  fname = req.body.fname
  lname = req.body.lname
  birthdate = req.body.birthdate
  group_id = req.body.group_id
  image_file = req.body.image_file
  contactClassIns.update(id,name,fname,lname,birthdate,group_id,image_file)
})
  //Phone
const phoneClassIns = new PhoneClass()
app.post('/ass2/phone/new',(req, res) => {
  phoneClassIns.create(req.body.contact_id,req.body.number)
})
  //Email
const emailClassIns = new EmailClass()
app.post('/ass2/email/new',(req, res) => {
  emailClassIns.create(req.body.contact_id,req.body.email)
})
  //Url
const urlClassIns = new UrlClass()
app.post('/ass2/url/new',(req, res) => {
  urlClassIns.create(req.body.contact_id,req.body.url)
})
////end assignment 2

////assignment 3
const ass3ClassIns = new Ass3Class()
app.post('/ass3/caltax',(req,res) => {
  let tax = ass3ClassIns.calculateTax(req.body.income)
  res.send(tax+"")
})
////end assignment 3

// starting the server
app.listen(3001, () => {
  console.log('listening on port 3001');
});
