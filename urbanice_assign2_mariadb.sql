-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 25, 2020 at 07:19 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `urbanice_assign2`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `image_file` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `fname`, `lname`, `birthdate`, `group_id`, `image_file`, `createdAt`, `updatedAt`) VALUES
(1, 'Hoy', 'Hoy', 'Hoo', '1982-09-11', 7, 'hoy.jpg', '2020-05-24 07:02:15', '2020-05-24 09:50:11'),
(3, 'Toy', 'Toy', 'Limp', '1980-02-24', 7, 'toy.webm', '2020-05-24 07:40:06', '2020-05-24 07:40:06'),
(6, 'Jojo', 'Jo', 'Jo', '2020-05-20', 8, 'jo.jpg', '2020-05-27 00:00:00', '2020-05-21 00:00:00'),
(7, 'Gohung', 'Gohung', 'Seiya', '1982-02-02', 7, 'gohung.jpg', '2020-05-18 00:00:00', '2020-05-24 11:41:47'),
(8, 'Poy', 'Poy', 'Limp', '1980-02-24', 7, 'Poy.jpg', '2020-05-24 09:47:03', '2020-05-24 09:47:03'),
(9, 'Goku', 'Goku', 'Seiya', '2019-02-02', 7, 'goku.jpg', '2020-05-24 11:38:54', '2020-05-24 11:38:54');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` bigint(20) NOT NULL,
  `contact_id` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `contact_id`, `email`, `createdAt`, `updatedAt`) VALUES
(5, 7, 'koko@hotmail.com', '2020-05-24 11:54:54', '2020-05-24 11:54:54'),
(6, 7, 'mawmeow@hotmail.com', '2020-05-24 11:55:23', '2020-05-24 11:55:23');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `createdAt`, `updatedAt`) VALUES
(7, 'Land', '2020-05-24 06:28:17', '2020-05-24 11:09:31'),
(8, 'Handsome', '2020-05-20 00:00:00', '2020-05-21 00:00:00'),
(9, 'Sea', '2020-05-24 10:03:43', '2020-05-24 10:03:43'),
(10, 'Wood', '2020-05-24 10:04:19', '2020-05-24 10:04:19'),
(12, 'Water', '2020-05-24 11:11:13', '2020-05-24 11:11:13');

-- --------------------------------------------------------

--
-- Table structure for table `phones`
--

CREATE TABLE `phones` (
  `id` bigint(20) NOT NULL,
  `contact_id` bigint(20) NOT NULL,
  `number` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phones`
--

INSERT INTO `phones` (`id`, `contact_id`, `number`, `createdAt`, `updatedAt`) VALUES
(2, 1, '0831103806', '2020-05-24 08:20:59', '2020-05-24 08:20:59'),
(3, 1, '083999999', '2020-05-24 08:20:59', '2020-05-24 08:20:59'),
(4, 7, '0891111111', '2020-05-24 11:53:07', '2020-05-24 11:53:07');

-- --------------------------------------------------------

--
-- Table structure for table `urls`
--

CREATE TABLE `urls` (
  `id` bigint(20) NOT NULL,
  `contact_id` bigint(20) NOT NULL,
  `url` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `urls`
--

INSERT INTO `urls` (`id`, `contact_id`, `url`, `createdAt`, `updatedAt`) VALUES
(2, 1, 'www.latetx.com', '2020-05-24 08:41:11', '2020-05-24 08:41:11'),
(3, 1, 'www.lxxx.com', '2020-05-24 08:44:57', '2020-05-24 08:44:57'),
(4, 7, 'www.hotmail.com', '2020-05-24 11:56:44', '2020-05-24 11:56:44'),
(5, 7, 'www.bcdham.com', '2020-05-24 11:57:02', '2020-05-24 11:57:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `urls`
--
ALTER TABLE `urls`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `phones`
--
ALTER TABLE `phones`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `urls`
--
ALTER TABLE `urls`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
